package com.theecourt.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theecourt.backend.domain.Client;
import com.theecourt.backend.domain.ClientRepository;
import com.theecourt.backend.infrastructure.BaseServiceImpl;

@Service
public class ClientServiceImpl extends BaseServiceImpl<Client, Long> implements ClientService {

	@Autowired
	public ClientServiceImpl(ClientRepository repository){
		super(repository,repository);
		this.repository = repository;
	}
}
