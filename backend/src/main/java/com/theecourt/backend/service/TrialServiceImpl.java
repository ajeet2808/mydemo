package com.theecourt.backend.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theecourt.backend.domain.Trial;
import com.theecourt.backend.domain.TrialRepository;
import com.theecourt.backend.infrastructure.BaseServiceImpl;

@Service
public class TrialServiceImpl extends BaseServiceImpl<Trial, Long> implements TrialService {

	@Autowired
	public TrialServiceImpl(TrialRepository repository) {
		super(repository,repository);
	}
}