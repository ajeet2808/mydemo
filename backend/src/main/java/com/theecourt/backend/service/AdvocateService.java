package com.theecourt.backend.service;

import com.theecourt.backend.domain.Advocate;
import com.theecourt.backend.infrastructure.BaseService;

public interface AdvocateService extends BaseService<Advocate,Long> {
	
}
