package com.theecourt.backend.service;

import com.theecourt.backend.domain.Hearing;
import com.theecourt.backend.infrastructure.BaseService;

public interface HearingService extends BaseService<Hearing,Long>{

}
