package com.theecourt.backend.service;

import com.theecourt.backend.domain.Trial;
import com.theecourt.backend.infrastructure.BaseService;

public interface TrialService extends BaseService<Trial,Long>{

}
