package com.theecourt.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theecourt.backend.domain.Hearing;
import com.theecourt.backend.domain.HearingRepository;
import com.theecourt.backend.infrastructure.BaseServiceImpl;

@Service
public class HearingServiceImpl extends BaseServiceImpl<Hearing, Long> implements HearingService {

	@Autowired
	public HearingServiceImpl(HearingRepository repository) {
		super(repository,repository);
	}
}