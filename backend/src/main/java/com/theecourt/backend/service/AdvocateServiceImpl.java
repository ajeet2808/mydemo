package com.theecourt.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theecourt.backend.domain.Advocate;
import com.theecourt.backend.domain.AdvocateRepository;
import com.theecourt.backend.infrastructure.BaseServiceImpl;

@Service
public class AdvocateServiceImpl extends BaseServiceImpl<Advocate, Long> implements AdvocateService {

	@SuppressWarnings("unused")
	private AdvocateRepository repository;

	@Autowired
	public AdvocateServiceImpl(AdvocateRepository repository){
		super(repository,repository);
		this.repository = repository;
	}
}
