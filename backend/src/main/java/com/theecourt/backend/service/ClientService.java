package com.theecourt.backend.service;

import com.theecourt.backend.domain.Client;
import com.theecourt.backend.infrastructure.BaseService;

public interface ClientService extends BaseService<Client,Long> {
	
}
