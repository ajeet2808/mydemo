package com.theecourt.backend.infrastructure;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.querydsl.core.types.Predicate;

public class BaseServiceImpl<T, ID extends Serializable> {

	protected JpaRepository<T,ID> repository;
	protected QueryDslPredicateExecutor<T> predicateExecuteor;

	public BaseServiceImpl(JpaRepository<T,ID> repository,QueryDslPredicateExecutor<T> predicateExecutor) {
		this.repository = repository;
		this.predicateExecuteor = predicateExecutor;
	}

	public T save(T model) {
		return repository.save(model);
	}

	public Iterable<T> save(Iterable<T> entities) {
		return repository.save(entities);
	}

	public Iterable<T> findAll() {
		return repository.findAll();
	}

	public T findOne(ID id) {
		return repository.findOne(id);
	}
	
	public T findOne(Predicate predicate){
		return predicateExecuteor.findOne(predicate);
	}
	
	public Iterable<T> findAll(Predicate predicate){
		return predicateExecuteor.findAll(predicate);
	}
}
