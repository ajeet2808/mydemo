package com.theecourt.backend.infrastructure;

import java.io.Serializable;

import com.querydsl.core.types.Predicate;

public class BaseControllerImpl<T, ID extends Serializable> {

	public BaseControllerImpl(BaseService<T, ID> service) {
		super();
		this.service = service;
	}

	protected BaseService<T, ID> service;

	public Iterable<T> finalAll() {
		return service.findAll();
	}

	public T save(T entity) {
		return service.save(entity);
	}

	public T findOne(ID id) {
		return service.findOne(id);
	}

	public Iterable<T> findAll() {
		return service.findAll();
	}

	public Iterable<T> findAll(Predicate p) {
		return service.findAll(p);
	}

	public T findOne(Predicate p) {
		return service.findOne(p);
	}
}
