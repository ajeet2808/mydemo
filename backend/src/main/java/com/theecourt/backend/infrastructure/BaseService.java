package com.theecourt.backend.infrastructure;

import java.io.Serializable;

import com.querydsl.core.types.Predicate;

public interface BaseService<T, ID extends Serializable> {

	public T save(T model);

	public Iterable<T> save(Iterable<T> entities);

	public Iterable<T> findAll();
	
	public Iterable<T> findAll(Predicate predicate);
	
	public T findOne(Predicate predicate);

	public T findOne(ID id);
}
