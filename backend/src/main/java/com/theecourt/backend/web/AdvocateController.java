package com.theecourt.backend.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.theecourt.backend.domain.Advocate;
import com.theecourt.backend.domain.QAdvocate;
import com.theecourt.backend.infrastructure.BaseControllerImpl;
import com.theecourt.backend.service.AdvocateService;

@RestController
@RequestMapping("advocates")
public class AdvocateController extends BaseControllerImpl<Advocate, Long> {
	
	@Autowired
	public AdvocateController(AdvocateService service) {
		super(service);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Advocate Post(@RequestBody Advocate advocate) {
		return save(advocate);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Advocate Put(@PathVariable Long id, @RequestBody Advocate advocate) {
		return save(advocate);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Iterable<Advocate> finaAll() {
		return findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Advocate finaOne(@PathVariable Long id) {
		return findOne(id);
	}
	
	@RequestMapping(value = "/firstName/{firstName}", method = RequestMethod.GET)
	public Advocate finaOne(@PathVariable String firstName) {
		Predicate p = QAdvocate.advocate.profile.firstName.toLowerCase().contains(firstName.toLowerCase());
		return findOne(p);
	}
}
