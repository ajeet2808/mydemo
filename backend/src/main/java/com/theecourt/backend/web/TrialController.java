package com.theecourt.backend.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theecourt.backend.domain.Trial;
import com.theecourt.backend.infrastructure.BaseControllerImpl;
import com.theecourt.backend.service.TrialService;

@RestController
@RequestMapping("trials")
public class TrialController extends BaseControllerImpl<Trial, Long> {
	
	@Autowired
	public TrialController(TrialService service) {
		super(service);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Trial Post(@RequestBody Trial trial) {
		return save(trial);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Trial Put(@PathVariable Long id, @RequestBody Trial trial) {
		return save(trial);
	}
}
