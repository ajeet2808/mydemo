package com.theecourt.backend.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.theecourt.backend.domain.Hearing;
import com.theecourt.backend.infrastructure.BaseControllerImpl;
import com.theecourt.backend.service.HearingService;

@RestController
@RequestMapping("hearings")
public class HearingController extends BaseControllerImpl<Hearing, Long> {
	@Autowired
	public HearingController(HearingService service) {
		super(service);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Hearing Post(@RequestBody Hearing trial) {
		return save(trial);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Hearing Put(@PathVariable Long id, @RequestBody Hearing trial) {
		return save(trial);
	}
}
