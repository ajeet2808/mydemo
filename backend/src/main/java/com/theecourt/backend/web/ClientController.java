package com.theecourt.backend.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.Predicate;
import com.theecourt.backend.domain.Client;
import com.theecourt.backend.domain.QClient;
import com.theecourt.backend.infrastructure.BaseControllerImpl;
import com.theecourt.backend.service.ClientService;

@RestController
@RequestMapping("clients")
public class ClientController extends BaseControllerImpl<Client, Long> {
	
	@Autowired
	public ClientController(ClientService service) {
		super(service);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Client Post(@RequestBody Client client) {
		return save(client);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Client Put(@PathVariable Long id, @RequestBody Client client) {
		return save(client);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Iterable<Client> finaAll() {
		return findAll();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Client finaOne(@PathVariable Long id) {
		return findOne(id);
	}
	
	@RequestMapping(value = "/firstName/{firstName}", method = RequestMethod.GET)
	public Client finaOne(@PathVariable String firstName) {
		Predicate p = QClient.client.profile.firstName.toLowerCase().contains(firstName.toLowerCase());
		return findOne(p);
	}
}
