package com.theecourt.backend.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name="Trials")
public class Trial {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="trial_id")
	private Long trialId;
	
	private String externalCode;
	
	private String internaCode;
	
	private String name;
	
	private String description;
	
	@Column(name="advocate_id")
	private Long advocateId;
	
	@Column(name="client_id")
	private Long clientId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="advocate_id",insertable = false, updatable = false)
	private Advocate advocate;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="client_id",insertable = false, updatable = false)
	private Client client;

	public Long getTrialId() {
		return trialId;
	}

	public void setTrialId(Long trialId) {
		this.trialId = trialId;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getInternaCode() {
		return internaCode;
	}

	public void setInternaCode(String internaCode) {
		this.internaCode = internaCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAdvocateId() {
		return advocateId;
	}

	public void setAdvocateId(Long advocateId) {
		this.advocateId = advocateId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Advocate getAdvocate() {
		return advocate;
	}

	public void setAdvocate(Advocate advocate) {
		this.advocate = advocate;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
}
