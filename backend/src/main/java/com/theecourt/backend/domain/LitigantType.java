/**
 * 
 */
package com.theecourt.backend.domain;

/**
 * @author ajeet
 *
 */
public enum LitigantType {
	CLAIMANT(1), DEFENDANT(2);
	
	private int value;
	
	LitigantType(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public static LitigantType parse(int id){
		LitigantType litigantType = null;
		for(LitigantType type: LitigantType.values()){
			if(type.getValue()==id){
				litigantType = type;
				break;
			}
		}
		return litigantType;
	}
}
