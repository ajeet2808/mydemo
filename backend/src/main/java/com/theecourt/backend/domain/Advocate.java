package com.theecourt.backend.domain;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity(name = "Advocates")
public class Advocate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="advocate_id")
	private Long advocateId;	

	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="profile_id")
	private Profile profile;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "advocate", targetEntity = Trial.class)
	private Collection<Trial> trials;

	public Long getAdvocateId() {
		return advocateId;
	}

	public void setAdvocateId(Long advocateId) {
		this.advocateId = advocateId;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Collection<Trial> getTrials() {
		return trials;
	}

	public void setTrials(Collection<Trial> trials) {
		this.trials = trials;
	}
}
