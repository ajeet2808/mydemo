package com.theecourt.backend.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "hearings")
public class Hearing {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long hearingId;

	private int hearingNumber;

	private Date hearingDate;

	private String notes;

	@Column(name = "trial_id")
	private Long trialId;

	@Column(name = "acovate_id")
	private Long advocateId;

	@Column(name = "client_id")
	private Long clientId;

	@ManyToOne(optional = false)
	@JoinColumn(name = "trial_id", referencedColumnName = "trial_id",insertable = false, updatable = false)
	private Trial trail;

	@ManyToOne(optional = false)
	@JoinColumn(name = "advocate_id", referencedColumnName = "advocate_id",insertable = false, updatable = false)
	private Advocate advocate;

	@ManyToOne(optional = false)
	@JoinColumn(name = "client_id", referencedColumnName = "client_id", insertable = false, updatable = false)
	private Client client;

	public Long getHearingId() {
		return hearingId;
	}

	public void setHearingId(Long hearingId) {
		this.hearingId = hearingId;
	}

	public int getHearingNumber() {
		return hearingNumber;
	}

	public void setHearingNumber(int hearingNumber) {
		this.hearingNumber = hearingNumber;
	}

	public Date getHearingDate() {
		return hearingDate;
	}

	public void setHearingDate(Date hearingDate) {
		this.hearingDate = hearingDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getTrialId() {
		return trialId;
	}

	public void setTrialId(Long trialId) {
		this.trialId = trialId;
	}

	public Long getAdvocateId() {
		return advocateId;
	}

	public void setAdvocateId(Long advocateId) {
		this.advocateId = advocateId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Trial getTrail() {
		return trail;
	}

	public void setTrail(Trial trail) {
		this.trail = trail;
	}

	public Advocate getAdvocate() {
		return advocate;
	}

	public void setAdvocate(Advocate advocate) {
		this.advocate = advocate;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
