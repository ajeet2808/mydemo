import {Component,Input} from '@angular/core';
import {Hearing,AdvocateService} from './../../shared/index';
import {HearingItemComponent} from './../index';

@Component({
    selector:'ec-hearing-list',
    templateUrl:'app/cases/components/hearing-list.component.html',
})
export
class HearingListComponent {
    @Input() hearings:Hearing[];
    constructor() {
    }
}