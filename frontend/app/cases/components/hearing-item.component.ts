import {Component,Input} from '@angular/core';
import {Hearing} from './../../shared/index';

@Component({
    selector:'ec-hearing-item',
    templateUrl:'app/cases/components/hearing-item.component.html'
})
export /**
 * HearingItemComponent
 */
class HearingItemComponent {
    @Input() hearing:Hearing;
    constructor() {
        
    }
}