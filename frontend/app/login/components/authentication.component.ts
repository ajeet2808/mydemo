import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {Form} from '@angular/forms';
import { AuthenticationService,User } from './../../shared/index';

@Component({
  selector: 'ec-auth',
  templateUrl: 'app/login/components/authentication.component.html',
  providers: [AuthenticationService]
})
export class AuthenticationComponent {
  email:string;
  password:string;
  isLoggedIn:boolean;
  user:User;
  constructor(public authService:AuthenticationService,private router:Router) {
    
  }
  login():void{
    this.authService.login(this.email,this.password,'Advocate')
    .subscribe(user=>{
      if(!!user){
        //console.log('LoggedIn:'+JSON.stringify(user));
        this.email = '';
        this.password = '';
        this.isLoggedIn = true;
        this.user = new User();
        Object.assign(this.user,user);
        this.router.navigate( ['/dashboard', user.id] );
      }else{
        this.logout();
      }
    });
  }
  logout():any{
      this.authService.logout().subscribe(resp=>{
        this.isLoggedIn = resp;
        this.router.navigate( ['/'] );
      });
    }
}
