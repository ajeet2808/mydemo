import { Component } from '@angular/core';
import { AuthenticationComponent } from './../login/index'; 

@Component({
  selector: 'sd-toolbar',
  templateUrl: 'app/components/toolbar.component.html',
  styleUrls: ['app/components/toolbar.component.css']
})
export class ToolbarComponent {}
