import { Component } from '@angular/core';
import {RouterOutlet} from '@angular/router';
import { NavbarComponent } from './navbar.component';
import { ToolbarComponent } from './toolbar.component';
import { NameListService } from '../shared/index';
import { HomeComponent } from '../home/index';
import { AboutComponent } from './../about';
import { DashboardComponent } from '../dashboard/index';
import {RegistrationComponent} from '../registration/index'

@Component({
  selector: 'sd-app',
  viewProviders: [NameListService],
  templateUrl: 'app/components/app.component.html'
})
export class AppComponent {}
