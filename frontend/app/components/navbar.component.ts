import { Component } from '@angular/core';
import {RouterLink} from '@angular/router';
import { AuthenticationService } from './../shared/index';

@Component({
  selector: 'sd-navbar',
  templateUrl: 'app/components/navbar.component.html',
  styleUrls: ['app/components/navbar.component.css']
})
export class NavbarComponent {
  
  constructor(public authService:AuthenticationService) {}
}
