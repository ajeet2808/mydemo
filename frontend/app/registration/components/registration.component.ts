import { Component } from '@angular/core';
import { RegistrationService,User } from './../../shared/index';

@Component({
  selector: 'ec-registration',
  templateUrl: 'app/registration/components/registration.component.html',
  providers: [RegistrationService]
})
export class RegistrationComponent {
  user:User;
  constructor(public regService:RegistrationService) {
    this.user = new User();
    this.user.userType = 'adv';
    this.user.name = {first:""};
  }
  register():void{
    this.regService.register(this.user)
    .subscribe(user=>{
      this.user.id=user.id;
    });
  }
  clear():void{
    this.user = new User();
    this.user.userType = "adv";
    this.user.name = {first:""};
  }
  isUserRegistered():boolean{
    return this.user.id!=null && this.user.id!=undefined;
  }
}
