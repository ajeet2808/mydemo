import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule,RouterOutlet,RouterLink} from '@angular/router';
import { AppComponent,ToolbarComponent,NavbarComponent }   from './components/index';
import {RegistrationComponent} from './registration/index';
import {AboutComponent} from './about/index';
import {DashboardComponent} from './dashboard/index';
import {AuthenticationComponent} from './login/index';
import {AuthenticationService,AdvocateService,GlobalService,NameListService,RegistrationService} from './shared/index';
import {HearingListComponent,HearingItemComponent} from './cases/index';

@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
        {
          path: '',
          component: RegistrationComponent
        },
        {
          path: 'about',
          component: AboutComponent 
        },
        {
          path: 'dashboard/:userId',
          component: DashboardComponent
        }
      ])
   ],

  declarations: [
        AppComponent,
        RegistrationComponent,
        DashboardComponent,
        ToolbarComponent,
        AboutComponent,
        NavbarComponent,
        AuthenticationComponent,
        HearingListComponent,
        HearingItemComponent
    ],
providers:[
    AuthenticationService,
    GlobalService,
    AdvocateService,
    NameListService,
    RegistrationService
],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
