import { Params, Router, ActivatedRoute } from '@angular/router';
import {Component,Input,OnInit} from '@angular/core';
import {HearingListComponent} from './../../cases/index';
import {AdvocateService,Hearing} from './../../shared/index';
@Component({
    selector:'ec-dashboard',
    templateUrl:'app/dashboard/components/dashboard.component.html'
})
export /**
DashboardComponent
 */
class DashboardComponent implements OnInit {
    public hearings:Hearing[];
    constructor(private advocateService:AdvocateService,
        private route: ActivatedRoute,
        private location: Location) {
        
    }
    
    ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
        let id = params['id'];
        // var date = new Date('07-03-2016');
        var dd = '03';//date.getDate();
        var mm = '07';//date.getMonth();
        var yyyy = '2016';//date.getFullYear();
        var dateStr = mm+'-'+dd+'-'+yyyy;
        this.advocateService.listHearings(id,dateStr)
        .subscribe(hearings=>{
            this.hearings = hearings;
        });
  });
  }
}
