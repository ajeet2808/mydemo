import { Component } from '@angular/core';

import { NameListService, AuthenticationService } from '../../shared/index';


@Component({
  selector: 'sd-home',
  templateUrl: 'app/home/components/home.component.html',
  styleUrls: ['app/home/components/home.component.css'],
  providers:[AuthenticationService]
})
export class HomeComponent {
  newName: string;
  constructor(public nameListService: NameListService, public authService:AuthenticationService) {}

  /*
   * @param newname  any text as input.
   * @returns return false to prevent default form submit behavior to refresh the page.
   */
  addName(): boolean {
    this.nameListService.add(this.newName);
    this.newName = '';
    return false;
  }
}
