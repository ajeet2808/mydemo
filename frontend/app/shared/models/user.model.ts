export class User{
    id:string;
    email:string;
    name:{first:string,last?:string};
    password:string;
    address:string
    userType:string;
    fullName():String{
        return this.name.first + ' '+this.name.last;
    }
}