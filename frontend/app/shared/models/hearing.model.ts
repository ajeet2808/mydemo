export class Hearing{
    caseId:string;
    caseName:string;
    remarks:string;
    previous:Date;
    date:Date;
    next:Date;
    court:string;
}