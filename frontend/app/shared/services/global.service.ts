import {Component, Injectable} from '@angular/core';
import {Http, Response, RequestOptions, Headers, Request, RequestMethod} from '@angular/http';

@Injectable()
export class GlobalService {
public base_path: string;

constructor(public http: Http) {
    this.base_path = "http://localhost:3500/api/";
}

public getRequestOptions(options:{
    url: string,
    method:RequestMethod,
    body?:string
}): RequestOptions {
    var headers = new Headers();
    headers.append("Content-Type", 'application/json');
    var token = localStorage.getItem('token');
    if(token){
        headers.append("Authorization", 'Bearer ' + token);
    }
    return new RequestOptions({
        method: options.method,
        url: this.base_path+options.url,
        headers: headers,
        body:options.body||""
    });
 }
}