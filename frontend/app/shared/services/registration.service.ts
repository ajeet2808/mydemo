import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Http, Response, Headers, Request, RequestMethod} from '@angular/http';
import {GlobalService} from './global.service';
import {User} from './..'

@Injectable()
export class RegistrationService {
 
  constructor(private http:Http,private globalService: GlobalService) {
    
  }

  register(user:User):Observable<User>{
      console.log("entering register method");
      var url = "/api/advocates";
        
        var requestoptions = this.globalService.getRequestOptions({
            url:"advocates",
            method:RequestMethod.Post,
            body:JSON.stringify(user)
        });
        return this.http.request(new Request(requestoptions))
        .map(resp=>{
            var respObj = resp.json();
            console.log("Got from server: "+resp.text());
            return respObj;
        });
  }
}