import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers, Request, RequestMethod} from '@angular/http';
import {User} from './..'
import {GlobalService} from './../services/global.service'

@Injectable()
export class AuthenticationService {
  token: string;
  constructor(private http:Http,private globalService:GlobalService) {
    this.token = localStorage.getItem('token');
  }
  logout():Observable<boolean>{
      localStorage.removeItem('id_token');
      console.log('Logged out!!');
      return Observable.of(!!localStorage.getItem('id_token'));
  }
  login(email:String,password:String,role:String):Observable<User>{
      //console.log('email:'+email+' passowrd:'+password+" role:"+role);
      
        var requestoptions = this.globalService.getRequestOptions({
            url:"logins",
            method:RequestMethod.Post,
            body:JSON.stringify({"email":email,"password":password,"role":role})
        });
        
        return this.http.request(new Request(requestoptions))
        .map(resp=>{
           var respObj = resp.json();
            if(!!respObj['token']){
                localStorage.setItem('token',respObj['token']);
                //console.log("Got from server: "+resp.text());
                return respObj['user'];
            }
            return null;
        });
  }
}