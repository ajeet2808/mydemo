import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Http, Response, Headers, Request, RequestMethod} from '@angular/http';
import {GlobalService} from './global.service';
import {Hearing} from './..'

@Injectable()
export class AdvocateService {
 
  constructor(private http:Http,private globalService: GlobalService) {
    
  }

  listHearings(id:string,date:string):Observable<Hearing[]>{
      console.log("entering register method");
      var url = "advocates/"+id+'/hearings/'+date;
        
        var requestoptions = this.globalService.getRequestOptions({
            url:url,
            method:RequestMethod.Get
        });
        
        return this.http.request(new Request(requestoptions))
        .map(resp=>{
            var respObj = resp.json();
            console.log("Got from server: "+resp.text());
            return respObj;
        });
  }
}